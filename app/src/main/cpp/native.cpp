#include <jni.h>
#include <algorithm>

//
// Created by User on 10.06.2020.
//

extern "C" JNIEXPORT jstring JNICALL
Java_dev_yakub_msolab_NativeActivity_sayHello(JNIEnv *env, jobject thiz) {
    return env->NewStringUTF("Hello world!");
}

extern "C"
JNIEXPORT void JNICALL
Java_dev_yakub_msolab_NativeActivity_sort(JNIEnv *env, jobject thiz, jintArray array) {
    size_t n = env->GetArrayLength(array);
    jint *elems = env->GetIntArrayElements(array, NULL);

    for (size_t i = 0; i < n - 1; i++) {
        for (size_t j = 0; j < n - i - 1; j++) {
            if (elems[j] > elems[j+1]) {
                std::swap(elems[j], elems[j+1]);
            }
        }
    }

    env->ReleaseIntArrayElements(array, elems, 0);
}

