package dev.yakub.msolab

import android.app.AlertDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class DrawingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawing)
    }

    override fun onBackPressed() {
        val dialog = AlertDialog.Builder(this).apply {
            setTitle("Go back?")
            setPositiveButton("Yes") { _, _ -> super.onBackPressed() }
            setNegativeButton("No") { _, _ -> }
        }.create()
        dialog.show()
    }
}