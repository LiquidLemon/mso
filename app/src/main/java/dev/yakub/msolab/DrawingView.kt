package dev.yakub.msolab

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.DragEvent
import android.view.MotionEvent
import android.view.View

class DrawingView : View {
    private val points = mutableListOf<Point>()
    private val brush = RectF(0f, 0f, 5f, 5f)
    private val paint = Paint().apply {
        setARGB(255, 255, 0, 0)
        strokeWidth = 5f
    }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)

        points.forEach {
            brush.offsetTo(it.x.toFloat(), it.y.toFloat())
            canvas?.drawOval(brush, paint)
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        event?.let {
            points.add(Point(it.x.toInt(), it.y.toInt()))
        }
        invalidate()

        return true
    }

//    override fun onDragEvent(event: DragEvent?): Boolean {
//        event?.let {
//            points.add(Point(it.x.toInt(), it.y.toInt()))
//        }
//        invalidate()
//
//        return true
//    }

}
