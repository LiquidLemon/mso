package dev.yakub.msolab

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.TextView
import android.widget.Toast

class FibonacciActivity : AppCompatActivity() {
    private var service: Messenger? = null
    private var isBound: Boolean = false

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            this@FibonacciActivity.service = Messenger(service)
            isBound = true
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            this@FibonacciActivity.service = null
            isBound = false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fibonacci)
    }

    override fun onStart() {
        super.onStart()

        Intent(this, FibonacciService::class.java).also { intent ->
            val ok = bindService(intent, connection, Context.BIND_AUTO_CREATE)
            if (!ok) {
                Toast.makeText(this, "Failed to bind activity", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onStop() {
        super.onStop()

        if (isBound) {
            unbindService(connection)
            isBound = false
        }
    }

    private val responseHandler = Handler {
        findViewById<TextView>(R.id.numberView).apply {
            text = it.arg1.toString()
        }
        true
    }

    fun getFibonacci(view: View) {
        if (!isBound) {
            Toast.makeText(this, "Not bound", Toast.LENGTH_SHORT).show()
            return
        }

        val msg = Message.obtain(null, MSG_GET_FIBONACCI, 0, 0).apply {
            replyTo = Messenger(responseHandler)
        }

        try {
            service?.send(msg)
        } catch (e: RemoteException) {
            Toast.makeText(this, e.stackTrace.joinToString(separator = "\n"), Toast.LENGTH_SHORT)
                .show()
        }
    }
}