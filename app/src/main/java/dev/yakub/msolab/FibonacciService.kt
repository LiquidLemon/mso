package dev.yakub.msolab

import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.os.Messenger

const val MSG_GET_FIBONACCI = 1

class FibonacciService : Service() {
    private lateinit var messenger: Messenger

    var a = 1
    var b = 1

    internal class IncomingHandler(private val context: FibonacciService) : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                MSG_GET_FIBONACCI -> {
                    msg.replyTo.send(Message.obtain(null, 0, context.b, 0))
                    val tmp = context.a
                    context.a += context.b
                    context.b = tmp
                }
                else -> super.handleMessage(msg)
            }

            super.handleMessage(msg)
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        messenger = Messenger(IncomingHandler(this))
        return messenger.binder
    }

}