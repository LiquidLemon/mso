package dev.yakub.msolab

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun startDrawing(view: View) {
        startActivity(Intent(this, DrawingActivity::class.java))
    }

    fun startNotes(view: View) {
        startActivity(Intent(this, NotesActivity::class.java))
    }

    fun startFibonacci(view: View) {
        startActivity(Intent(this, FibonacciActivity::class.java))
    }

    fun startMetrics(view: View) {
        startActivity(Intent(this, MetricsActivity::class.java))
    }

    fun startNative(view: View) {
        startActivity(Intent(this, NativeActivity::class.java))
    }
}