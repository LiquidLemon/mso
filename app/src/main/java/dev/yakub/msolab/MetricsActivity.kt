package dev.yakub.msolab

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Bundle
import android.text.format.Formatter
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.http.CacheControl
import io.ktor.http.ContentType
import io.ktor.response.cacheControl
import io.ktor.response.respondBytes
import io.ktor.response.respondTextWriter
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.broadcast
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.delay

const val port = 8080

class MetricsActivity : AppCompatActivity() {
    private lateinit var server: ApplicationEngine
    private lateinit var sensorManager: SensorManager
    private var gyroValues: List<Float> = emptyList()
    private lateinit var address: String
    private var running = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_metrics)
    }

    @ExperimentalCoroutinesApi
    override fun onStart() {
        super.onStart()

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)?.also { sensor ->
            sensorManager.registerListener(object : SensorEventListener {
                override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
                    /* no-op */
                }

                override fun onSensorChanged(event: SensorEvent?) {
                    event?.values?.toList()?.also {
                        gyroValues = it
                    }
                }
            }, sensor, SensorManager.SENSOR_DELAY_NORMAL)
        }

        if (running) {
            return
        }
        server = embeddedServer(Netty, port = port) {
            install(CORS) {
                anyHost()
            }

            val channel = produce {
                var n = 0
                while (true) {
                    send(
                        Event(
                            gyroValues.joinToString(
                                separator = ",",
                                prefix = "[",
                                postfix = "]"
                            ), id = n.toString()
                        )
                    )
                    n++
                    delay(1000)
                }
            }.broadcast()

            routing {
                get("/") {
                    resources.openRawResource(R.raw.index).readBytes().also {
                        call.respondBytes(it)
                    }
                }

                get("/metrics") {
                    val events = channel.openSubscription()
                    try {
                        call.respondSSE(events)
                    } finally {
                        events.cancel()
                    }
                }
            }
        }
        server.start()
        running = true

        val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val ip = Formatter.formatIpAddress(wifiManager.connectionInfo.ipAddress)
        address = "$ip:$port"

        findViewById<TextView>(R.id.addressView)?.apply {
            text = address
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        server.stop(0, 0)
        running = false
    }

    fun openClient(view: View) {
         startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://$address")))
    }
}

data class Event(val data: String, val id: String? = null)

suspend fun ApplicationCall.respondSSE(events: ReceiveChannel<Event>) {
    response.cacheControl(CacheControl.NoCache(null))
    respondTextWriter(contentType = ContentType.Text.EventStream) {
        for (event in events) {
            if (event.id != null) {
                write("id: ${event.id}\n")
            }

            for (line in event.data.lines()) {
                write("data: $line\n")
            }

            write("\n")
            flush()
        }
    }
}