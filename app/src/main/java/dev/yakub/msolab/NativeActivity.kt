package dev.yakub.msolab

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView


class NativeActivity : AppCompatActivity() {
    init {
        System.loadLibrary("native")
    }

    private external fun sayHello(): String
    private external fun sort(array: IntArray)

    private val numbers : IntArray = intArrayOf(15, 2, 5, 1, 10, 8, 3, 1, 20)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_native)

        findViewById<TextView>(R.id.stringView)?.apply {
            text = sayHello()
        }

        findViewById<TextView>(R.id.unsortedView)?.apply {
            text = numbers.joinToString()
        }

        sort(numbers)

        findViewById<TextView>(R.id.sortedView)?.apply {
            text = numbers.joinToString()
        }
    }
}
