package dev.yakub.msolab

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class NotesActivity : AppCompatActivity() {
    private lateinit var notesView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private val notes = mutableListOf<Note>()
    private lateinit var db: SQLiteDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)

        viewManager = LinearLayoutManager(this)
        viewAdapter = NotesAdapter(notes)

        notesView = findViewById<RecyclerView>(R.id.notesView).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        db = openOrCreateDatabase("notes", Context.MODE_PRIVATE, null)
        db.execSQL("CREATE TABLE IF NOT EXISTS NOTES (date DATETIME PRIMARY KEY DEFAULT CURRENT_TIMESTAMP, content VARCHAR)")
        updateNotes()
    }

    private fun updateNotes() {
        notes.clear()
        val cursor = db.query("NOTES", arrayOf("date", "content"), null, null, null, null, "date")

        while (cursor.moveToNext()) {
            notes.add(Note(cursor.getString(0), cursor.getString(1)))
        }

        cursor.close()
        viewAdapter.notifyDataSetChanged()
    }

    fun addNote(view: View) {
        val text = findViewById<EditText>(R.id.noteContent).text
        if (text.isEmpty()) {
            return
        }
        val values = ContentValues().apply {
            put("content", text.toString())
        }
        db.insertOrThrow("notes", null, values)
        text.clear()
        updateNotes()
    }
}

data class Note(val date: String, val content: String)

class NotesAdapter(private val notes: MutableList<Note>) :
    RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    class ViewHolder(val row: View) : RecyclerView.ViewHolder(row)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val row = LayoutInflater.from(parent.context).inflate(R.layout.note, parent, false)

        return ViewHolder(row)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.row.apply {
            findViewById<TextView>(R.id.dateView)?.apply {
                text = notes[position].date
            }

            findViewById<TextView>(R.id.contentView)?.apply {
                text = notes[position].content
            }
        }
    }

    override fun getItemCount() = notes.size
}
